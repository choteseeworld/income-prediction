import pandas as pd
import numpy as np

data = pd.DataFrame()
data['Gender'] = ['male','male','male','male','female','female','female','female']

data['Height'] = [180,178,165,179,150,160,158,165]
data['Weight'] = [80,85,75,78,45,55,52,61]
data['FootSize'] = [12,11,12,10,6,8,7,9]

#print(data)

person = pd.DataFrame()
person['Height'] = [180]
person['Weight'] = [60]
person['FootSize'] = [8]

n_male = data['Gender'][data['Gender'] == 'male'].count()
n_female = data['Gender'][data['Gender'] == 'female'].count()
n_total = data['Gender'].count()

p_male = n_male/n_total
p_female = n_female/n_total

data_means = data.groupby('Gender').mean()
data_variance = data.groupby('Gender').var()

#mean for male
male_height_mean = data_means['Height'][data_variance.index == 'male'].values[0]
male_weight_mean = data_means['Weight'][data_variance.index == 'male'].values[0]
male_footsize_mean = data_means['FootSize'][data_variance.index == 'male'].values[0]

#variance for male
male_height_var = data_variance['Height'][data_variance.index == 'male'].values[0]
male_weight_var = data_variance['Weight'][data_variance.index == 'male'].values[0]
male_footsize_var = data_variance['FootSize'][data_variance.index == 'male'].values[0]

#mean for female
female_height_mean = data_means['Height'][data_variance.index == 'female'].values[0]
female_weight_mean = data_means['Weight'][data_variance.index == 'female'].values[0]
female_footsize_mean = data_means['FootSize'][data_variance.index == 'female'].values[0]

#variance for female
female_height_var = data_variance['Height'][data_variance.index == 'female'].values[0]
female_weight_var = data_variance['Weight'][data_variance.index == 'female'].values[0]
female_footsize_var = data_variance['FootSize'][data_variance.index == 'female'].values[0]

# function compute Gaussian p(x|y)
def p_xy(x, mean_y, var_y):
	p=1/(np.sqrt(2*np.pi*var_y)) * np.exp((-(x-mean_y)**2)/(2*var_y))
	return p

p_male_given_data = p_male*\
p_xy(person['Height'][0], male_height_mean, male_height_var)*\
p_xy(person['Weight'][0], male_weight_mean, male_weight_var)*\
p_xy(person['FootSize'][0], male_footsize_mean, male_footsize_var)

p_female_given_data = p_female*\
p_xy(person['Height'][0], female_height_mean, female_height_var)*\
p_xy(person['Weight'][0], female_weight_mean, female_weight_var)*\
p_xy(person['FootSize'][0], female_footsize_mean, female_footsize_var)

print(p_male_given_data)
print(p_female_given_data)