import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn import linear_model
import statsmodels.api as sm

df = pd.read_csv('num_vs_temp.csv', parse_dates = ['date'])

Y = df['price']
X = df[['temp', 'number', 'age', 'avg_price']]

# with sklearn
regr = linear_model.LinearRegression()
regr.fit(X, Y)

print('Intercept: \n', regr.intercept_)
print('Coefficients: \n', regr.coef_)

# prediction with sklearn
# New_Temp_Rate
# New_Number_Rate
# New_Age_Rate
# New_avg_price_Rate
def predictor(New_Temp_Rate, New_Number_Rate, New_Age_Rate, New_avg_price_Rate):
    # return print ('Predicted price of commercial in next mount: \n', \
    #     regr.predict([[New_Temp_Rate ,New_Number_Rate, New_Age_Rate, New_avg_price_Rate]]))
    return regr.predict([[New_Temp_Rate ,New_Number_Rate, New_Age_Rate, New_avg_price_Rate]])
     

# predictor(40, 200, 6, 60000)
# # with statsmodels
# X = sm.add_constant(X) # adding a constant

# model = sm.OLS(Y, X).fit()
# predictions = model.predict(X) 

# print_model = model.summary()
# # print(print_model)