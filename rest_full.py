from flask import Flask, request, jsonify
from flask_restful import reqparse, abort, Api, Resource
from mutiple_linear import predictor
import simplejson as json
app = Flask(__name__)
api = Api(app)

TODOS = {
    'todo1': {'task': 'build an API'},
    'todo2': {'task': '?????'},
    'todo3': {'task': 'profit!'},
    'temp' : {'temp': 40}
}


def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))

parser = reqparse.RequestParser()
parser.add_argument('task')


# Todo
# shows a single todo item and lets you delete a todo item
class Todo(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return TODOS[todo_id]

    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        del TODOS[todo_id]
        return '', 204

    def put(self, todo_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        TODOS[todo_id] = task
        return task, 201


# TodoList
# shows a list of all todos, and lets you POST to add new tasks
class TodoList(Resource):
    def get(self):
        return TODOS

    def post(self):
        # args = parser.parse_args()
        print('this is args')
        temp = request.form['temp']
        number = request.form['number']
        age = request.form['age']
        price = request.form['price']
        avg_price = request.form['avg_price']


        data = {
			'data': str(predictor(float(temp), int(number), float(age), float(avg_price))).replace("[", "").replace("]", "")
		}

        response = app.response_class(
    		response=json.dumps(data),
		    mimetype='application/json'
    	)

        # print(args)
        # todo_id = int(max(TODOS.keys()).lstrip('todo')) + 1
        # todo_id = 'todo%i' % todo_id
        # TODOS[todo_id] = {'temp': args['temp']}
        
        return response
        # return 200

##
## Actually setup the Api resource routing here
##
api.add_resource(TodoList, '/todos')
api.add_resource(Todo, '/todos/<todo_id>')


if __name__ == '__main__':
    app.run(debug=True, port='5241')